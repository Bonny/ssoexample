package com.sso.example.util;

import java.util.HashSet;
import java.util.Set;

import org.apache.log4j.Logger;
import org.opensaml.DefaultBootstrap;
import org.opensaml.xml.ConfigurationException;

public class OpenSamlBootstrap extends DefaultBootstrap {

	private static Logger log = Logger.getLogger(OpenSamlBootstrap.class);
	private static boolean initialized;

	private static final Set<String> xmlToolingConfigs = new HashSet<>();

	static {
		xmlToolingConfigs.add("/default-config.xml");
		xmlToolingConfigs.add("/encryption-validation-config.xml");
		xmlToolingConfigs.add("/saml2-assertion-config.xml");
		xmlToolingConfigs.add("/saml2-assertion-delegation-restriction-config.xml");
		xmlToolingConfigs.add("/saml2-core-validation-config.xml");
		xmlToolingConfigs.add("/saml2-metadata-config.xml");
		xmlToolingConfigs.add("/saml2-metadata-idp-discovery-config.xml");
		xmlToolingConfigs.add("/saml2-metadata-query-config.xml");
		xmlToolingConfigs.add("/saml2-metadata-validation-config.xml");
		xmlToolingConfigs.add("/saml2-protocol-config.xml");
		xmlToolingConfigs.add("/saml2-protocol-thirdparty-config.xml");
		xmlToolingConfigs.add("/schema-config.xml");
		xmlToolingConfigs.add("/signature-config.xml");
		xmlToolingConfigs.add("/signature-validation-config.xml");
	};

	public static synchronized void init() {
		if (!initialized) {
			try {
				initializeXMLTooling(xmlToolingConfigs.toArray(new String[xmlToolingConfigs.size()]));
			} catch (ConfigurationException e) {
				log.error("Unable to initialize opensaml DefaultBootstrap", e);
			}
			initializeGlobalSecurityConfiguration();
			initialized = true;
		}
	}
}